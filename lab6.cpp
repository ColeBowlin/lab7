#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs); //Function Prototype

//bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));


  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
	employee employeeArray[10];

	for(int i = 0; i < 10; i ++){
		cout << "Enter employee #" << i+1 << "'s first name: ";
		cin >> employeeArray[i].firstName;
		cout << "Enter employee #" << i+1 << "'s last name: ";
		cin >> employeeArray[i].lastName;
		cout << "Enter employee #" << i+1 << "'s birth year: ";
		cin >> employeeArray[i].birthYear;
		cout << "Enter employee #" << i+1 << "'s hourly wage: ";
		cin >> employeeArray[i].hourlyWage;
	}

  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	 employee* start = employeeArray;
	 employee* end = employeeArray + 10;
	 random_shuffle(start,end,myrandom);

   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
		employee smallArray[5] = {employeeArray[0],employeeArray[1],employeeArray[2],employeeArray[3],employeeArray[4]};

    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
		 start = smallArray;
		 end = smallArray + 5;
		 sort(start,end,name_order);

    /*Now print the array below */
		for(int i = 0; i < 5; i++){
			cout << right << setw(30) << smallArray[i].lastName + ", " + smallArray[i].firstName << endl;
			cout << right << setw(30) << smallArray[i].birthYear << endl;
			cout << right << setw(30) << setprecision(2)<< smallArray[i].hourlyWage << endl;
		}



  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
   return lhs.lastName < rhs.lastName;
}
